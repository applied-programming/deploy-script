var casper = require('casper').create({
  clientScripts: ['config.js'],
  pageSettings: {
    loadImages: false,
    loadPlugins: false
  },
  logLevel: "info",
  verbose: false
});
var userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X)';
casper.userAgent(userAgent);

var fs = require('fs');
var currentFile = require('system').args[3];
var curFilePath = fs.absolute(currentFile).split('/');
var curDirPath = "C:\\";
if (curFilePath.length > 1) {
  curFilePath.pop(); // PhantomJS does not have an equivalent path.baseName()-like method
  curDirPath = curFilePath.join('\\');
}

// 設定（config.jsから読込み）
var config = {};

// Login Page
var applicanLoginUri = 'https://user.applican.com/Login/timeout';
casper.start(applicanLoginUri, function() {

  // 設定の読込み
  config = casper.evaluate(function() {
    return config;
  });
  config.filePath = curDirPath + '\\' + config.filePath;

  if ( this.getCurrentUrl() != projectIndexUri ) {
    // ログイン処理
    this.fillSelectors('form#frm', {
      'input[name="login_id"]': config.userName,
      'input[name="login_pswd"]': config.userPassword
    }, true);
  }
});

casper.then(function() {
  // ページの自動遷移のため
});

// Project Pages
var projectIndexUri = 'https://user.applican.com/project/index';

// Project List Page
casper.then(function() {
  if (this.getCurrentUrl() != projectIndexUri) {
    this.echo("Login filed!!");
    this.exit();
  }
  this.echo("1. Login success!!");

  var link = 'a.btn_main_clr.btn_wide[href="/project/' + config.projectId + '/info/top"]';
  this.click(link);
});

// Project Top Page
casper.then(function() {
  var projectTopUri = 'https://user.applican.com/project/' + config.projectId + '/info/top';
  if (this.getCurrentUrl() != projectTopUri) {
    this.echo("Cannot reach Project Top Page!!");
    this.exit();
  }
  this.echo("2. Project Top Page!!");

  var link = 'a.btn_main_clr.btn_large[href="/project/' + config.projectId + '/app_data/upload"]';
  this.click(link);
});

// Upload Page
casper.then(function() {
  var projectUploadUri = 'https://user.applican.com/project/' + config.projectId + '/app_data/upload';
  if (this.getCurrentUrl() != projectUploadUri) {
    this.echo("Cannot reach Upload Page!!");
    this.exit();
  }
  this.echo("3. Project Upload Page!!");

  if (this.exists('input#file_name')) {
    this.click('label#drap_switch');
    this.fill('form#frm', {
      'data[file]': config.filePath
    }, true);
    this.click('a.btn_round_blue[href="javascript:np.submit(\'#frm\');"]');
  }
});

casper.then(function() {
  // ページの自動遷移のため
});

casper.then(function() {
  var projectUploadHistoryUri = 'https://user.applican.com/project/' + config.projectId + '/app_data/history';
  if (this.getCurrentUrl() != projectUploadHistoryUri) {
    this.echo("Upload failed!!");
    this.exit();
  }
  this.echo("4. Upload success!!");
});

casper.run();


アプリカンのプロジェクト用のデプロイスクリプト
自動的にコードフォルダをZIPにアーカイブし，アプリカンのWebページにアップロードする．

0. ファイルの内容

  README.txt	- 簡単な説明
  config.js	- 設定ファイル
  deploy.js	- アップロードスクリプト
  deploy.bat	- 起動ファイル

1. 必要なソフトウェア
以下のソフトウェアをインストールして下さい．

1.1 CasperJS (http://casperjs.org/)
CasperJSはインストールの上，環境変数Pathを設定して下さい．

1.2 7-Zip コンソールバージョン (http://www.7-zip.org/)
ダウンロードして，アーカイブに含まれる 7za.exe を同一ディレクトリに配置する．

2. 配置
以下のようにアプリカンのコードを置いたディレクトリに並べてスクリプトファイルを配置する．

  web/		- アプリカンのコードを置いたディレクトリ
  README.txt	- 簡単な説明
  config.js	- 設定ファイル
  deploy.js	- アップロードスクリプト
  deploy.bat	- 起動ファイル

3. 設定
設定ファイル (config.js) の内容を記入する．
設定項目は
　・ ユーザ名
	アプリカンのWebページ(https://user.applican.com/)にログインするユーザ名
　・ パスワード
	アプリカンのWebページ(https://user.applican.com/)にログインするパスワード
　・ プロジェクトID
	アプリカンのWebページ(https://user.applican.com/)に登録されたプロジェクトID
　・ アップロードするZIPファイルのパス
	デフォルトでは web.zip （スクリプトの設置ディレクトリからの相対パス）

  var config = {
    userName: "username",
    userPassword: "password",
    projectId: 9999,
    filePath: "web.zip"
  };

3. 実行
deploy.bat を実行する．
